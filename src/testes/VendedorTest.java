package testes;

import implementacao.VendedorImpl;
import interfaces.Estoque;
import interfaces.Expedicao;
import interfaces.Vendedor;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

public class VendedorTest {
	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery();

	@Test
	public void testAprovarPedidoRejeitadoEstoque() {

		Vendedor v = new VendedorImpl();
		
		final Estoque estoque = context.mock(Estoque.class);
		final Expedicao expedicao = context.mock(Expedicao.class);
		
		v.setEstoque(estoque);
		v.setExpedicao(expedicao);
		
		context.checking(new Expectations(){{ 
			allowing(estoque).aprovarPedido(null); will(returnValue(false));
			allowing(expedicao).aprovarPedido(null); 
		}});
		
		boolean resultado = v.aprovaPedido(null);
		Assert.assertFalse(resultado);	
	}
	
	@Test
	public void testAprovarPedidoRejeitadoEstoqueRejeitadoExpedicao()
	{
		Vendedor v = new VendedorImpl();
		
		final Estoque estoque = context.mock(Estoque.class);
		final Expedicao expedicao = context.mock(Expedicao.class);
		
		v.setEstoque(estoque);
		v.setExpedicao(expedicao);
		
		context.checking(new Expectations(){{ 
			oneOf(estoque).aprovarPedido(null); will(returnValue(false));
			never(expedicao).aprovarPedido(null); will(returnValue(false));
		}});
		
		boolean resultado = v.aprovaPedido(null);
		Assert.assertFalse(resultado);	
		
	}

	@Test
	public void testAprovarPedidoAceitoEstoqueAceitoExpedicao()
	{
		Vendedor v = new VendedorImpl();
		
		final Estoque estoque = context.mock(Estoque.class);
		final Expedicao expedicao = context.mock(Expedicao.class);
		
		v.setEstoque(estoque);
		v.setExpedicao(expedicao);
		
		context.checking(new Expectations(){{ 
			allowing(estoque).aprovarPedido(null); will(returnValue(true));
			allowing(expedicao).aprovarPedido(null); will(returnValue(true));
		}});
		
		boolean resultado = v.aprovaPedido(null);
		Assert.assertTrue(resultado);	
		
	}
}
