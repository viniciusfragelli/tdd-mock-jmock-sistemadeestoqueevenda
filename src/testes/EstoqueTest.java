package testes;

import junit.framework.Assert;
import implementacao.EstoqueImpl;
import implementacao.Pedido;
import implementacao.Produto;
import interfaces.BaseDados;
import interfaces.Estoque;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Rule;
import org.junit.Test;

public class EstoqueTest {
	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery();

	@Test
	public void testAprovarPedidoTodosEmEstoque() {

		Pedido pedido = new Pedido();
		final Produto prod1 = new Produto();
		final Produto prod2 = new Produto();
		final Produto prod3 = new Produto();
		
		pedido.addItem(prod1,10);
		pedido.addItem(prod2,15);
		pedido.addItem(prod3,20);
		
		final BaseDados base = context.mock(BaseDados.class);
		
		context.checking(new Expectations(){{ 
			allowing(base).quantidade(prod1); will(returnValue(20));
			allowing(base).quantidade(prod2); will(returnValue(10));
			allowing(base).quantidade(prod3); will(returnValue(20));
		}});
		
		Estoque estoque = new EstoqueImpl();
		estoque.setBaseDados(base);
		
		boolean resultado = estoque.aprovarPedido(pedido);
		Assert.assertTrue(resultado);
			
	}
	
	@Test
	public void testAprovarPedidoItemFaltando() {

		Pedido pedido = new Pedido();
		final Produto prod1 = new Produto();
		final Produto prod2 = new Produto();
		final Produto prod3 = new Produto();
		
		pedido.addItem(prod1,10);
		pedido.addItem(prod2,15);
		pedido.addItem(prod3,20);
		
		final BaseDados base = context.mock(BaseDados.class);
		
		context.checking(new Expectations(){{ 
			allowing(base).quantidade(prod1); will(returnValue(20));
			allowing(base).quantidade(prod2); will(returnValue(10));
			allowing(base).quantidade(prod3); will(returnValue(20));
		}});
		
		Estoque estoque = new EstoqueImpl();
		estoque.setBaseDados(base);
		
		boolean resultado = estoque.aprovarPedido(pedido);
		Assert.assertFalse(resultado);
			
	}

	@Test
	public void testAprovarPedidoVazio() {

		Pedido pedido = new Pedido();
		
		
		final BaseDados base = context.mock(BaseDados.class);
		
		context.checking(new Expectations(){{ 
			never(base).quantidade(null); 
		}});
		
		Estoque estoque = new EstoqueImpl();
		estoque.setBaseDados(base);
		
		boolean resultado = estoque.aprovarPedido(pedido);
		Assert.assertFalse(resultado);
			
	}

}
