package interfaces;

import implementacao.Pedido;

public interface Estoque {
	
	public boolean aprovarPedido(Pedido p);

	public void setBaseDados(BaseDados base);

	
}
