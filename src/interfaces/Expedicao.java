package interfaces;

import implementacao.Pedido;

public interface Expedicao {

	public boolean aprovarPedido(Pedido p);
	
}
