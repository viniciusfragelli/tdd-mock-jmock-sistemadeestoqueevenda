package interfaces;

import implementacao.Pedido;

public interface Vendedor {

	public boolean aprovaPedido(Pedido p);
	public void setEstoque(Estoque e);
	public void setExpedicao(Expedicao expedicao);
}
