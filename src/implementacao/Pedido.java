package implementacao;

import java.util.HashMap;
import java.util.Map;



public class Pedido {

	private Map<Produto, Integer> itens = new HashMap<Produto, Integer>();
	
	public void addItem(Produto prod1, int i) {
		itens.put(prod1, i);
	}

	public Map<Produto,Integer> getItens() {
		return itens;
	}

}
