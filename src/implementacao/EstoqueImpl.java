package implementacao;

import interfaces.BaseDados;
import interfaces.Estoque;

import java.util.Set;

public class EstoqueImpl implements Estoque {

	private BaseDados base;
	
	@Override
	public boolean aprovarPedido(Pedido p) {
		Set<Produto> produtos = p.getItens().keySet();
		
		if(produtos == null || produtos.size()==0)
			return false;
		
		for(Produto prod : produtos)
		{
			int quantidadePedida = p.getItens().get(prod);
			int quantidadeEstoque = base.quantidade(prod);
			if(quantidadePedida > quantidadeEstoque)
				return false;
		}
		return true;
	}

	@Override
	public void setBaseDados(BaseDados base) {
		this.base = base;
	}

}
