package implementacao;

import interfaces.Estoque;
import interfaces.Expedicao;
import interfaces.Vendedor;

public class VendedorImpl implements Vendedor {

	private Estoque estoque;
	private Expedicao expedicao;
	
	@Override
	public boolean aprovaPedido(Pedido p) {

		if(estoque.aprovarPedido(p) && expedicao.aprovarPedido(p)   )
				return true;
			else
				return false;
	}

	@Override
	public void setEstoque(Estoque e) {
		estoque = e;
		
	}

	@Override
	public void setExpedicao(Expedicao expedicao) {
		this.expedicao = expedicao;
		
	}

}
